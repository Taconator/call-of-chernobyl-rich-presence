﻿namespace Taconator.CoCRP
{
    /// <summary>
    /// Instance Manager
    /// </summary>
    public static class IM
    {
        /// <summary>
        /// Main window instance
        /// </summary>
        public static Main MainWindow;
        /// <summary>
        /// Discord Rich Presence Controller instance
        /// </summary>
        public static Controller ControllerInstance;
        /// <summary>
        /// How frequently rich presence is updated, actual variable
        /// </summary>
        private static int _updateFrequency;
        /// <summary>
        /// How frequently rich presence is updated, property
        /// </summary>
        public static int UpdateFrequency
        {
            get
            {
                return _updateFrequency;
            }

            set
            {
                _updateFrequency = value;
                Properties.Settings.Default.UpdateFrequency = value;
                Properties.Settings.Default.Save();
            }
        }
        /// <summary>
        /// Constructor, gets and sets Update Frequency from settings
        /// </summary>
        static IM()
        {
            UpdateFrequency = Properties.Settings.Default.UpdateFrequency;
        }
    }
}
