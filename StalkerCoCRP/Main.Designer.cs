﻿namespace Taconator.CoCRP
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LogBox = new System.Windows.Forms.ListBox();
            this.StripTop = new System.Windows.Forms.MenuStrip();
            this.Debug = new System.Windows.Forms.ToolStripMenuItem();
            this.ClearLog = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitRP = new System.Windows.Forms.ToolStripMenuItem();
            this.ExitBoth = new System.Windows.Forms.ToolStripMenuItem();
            this.DebugSep = new System.Windows.Forms.ToolStripSeparator();
            this.UpdateFreqHeader = new System.Windows.Forms.ToolStripMenuItem();
            this.UpdateFreq = new System.Windows.Forms.ToolStripTextBox();
            this.ApplyUpdateFreq = new System.Windows.Forms.ToolStripMenuItem();
            this.stylingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Colors = new System.Windows.Forms.ToolStripMenuItem();
            this.FG = new System.Windows.Forms.ToolStripMenuItem();
            this.BG = new System.Windows.Forms.ToolStripMenuItem();
            this.FontItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StripTop.SuspendLayout();
            this.SuspendLayout();
            // 
            // LogBox
            // 
            this.LogBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LogBox.FormattingEnabled = true;
            this.LogBox.ItemHeight = 21;
            this.LogBox.Location = new System.Drawing.Point(0, 25);
            this.LogBox.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.LogBox.Name = "LogBox";
            this.LogBox.Size = new System.Drawing.Size(484, 436);
            this.LogBox.TabIndex = 0;
            // 
            // StripTop
            // 
            this.StripTop.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Debug,
            this.stylingToolStripMenuItem});
            this.StripTop.Location = new System.Drawing.Point(0, 0);
            this.StripTop.Name = "StripTop";
            this.StripTop.Padding = new System.Windows.Forms.Padding(9, 3, 0, 3);
            this.StripTop.Size = new System.Drawing.Size(484, 25);
            this.StripTop.TabIndex = 1;
            this.StripTop.Text = "menuStrip1";
            // 
            // Debug
            // 
            this.Debug.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ClearLog,
            this.ExitRP,
            this.ExitBoth,
            this.DebugSep,
            this.UpdateFreqHeader,
            this.UpdateFreq,
            this.ApplyUpdateFreq});
            this.Debug.Name = "Debug";
            this.Debug.Size = new System.Drawing.Size(54, 19);
            this.Debug.Text = "&Debug";
            // 
            // ClearLog
            // 
            this.ClearLog.Name = "ClearLog";
            this.ClearLog.Size = new System.Drawing.Size(225, 22);
            this.ClearLog.Text = "&Clear";
            this.ClearLog.Click += new System.EventHandler(this.ClearLog_Click);
            // 
            // ExitRP
            // 
            this.ExitRP.Name = "ExitRP";
            this.ExitRP.Size = new System.Drawing.Size(225, 22);
            this.ExitRP.Text = "Exit &RP";
            this.ExitRP.Click += new System.EventHandler(this.ExitRP_Click);
            // 
            // ExitBoth
            // 
            this.ExitBoth.Name = "ExitBoth";
            this.ExitBoth.Size = new System.Drawing.Size(225, 22);
            this.ExitBoth.Text = "&Exit RP and CoC";
            this.ExitBoth.Click += new System.EventHandler(this.ExitBoth_Click);
            // 
            // DebugSep
            // 
            this.DebugSep.Name = "DebugSep";
            this.DebugSep.Size = new System.Drawing.Size(222, 6);
            // 
            // UpdateFreqHeader
            // 
            this.UpdateFreqHeader.Name = "UpdateFreqHeader";
            this.UpdateFreqHeader.Size = new System.Drawing.Size(225, 22);
            this.UpdateFreqHeader.Text = "Update Frequency (Seconds)";
            // 
            // UpdateFreq
            // 
            this.UpdateFreq.BackColor = System.Drawing.Color.White;
            this.UpdateFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.UpdateFreq.Name = "UpdateFreq";
            this.UpdateFreq.Size = new System.Drawing.Size(100, 23);
            this.UpdateFreq.TextBoxTextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ApplyUpdateFreq
            // 
            this.ApplyUpdateFreq.Name = "ApplyUpdateFreq";
            this.ApplyUpdateFreq.Size = new System.Drawing.Size(225, 22);
            this.ApplyUpdateFreq.Text = "Apply";
            this.ApplyUpdateFreq.Click += new System.EventHandler(this.ApplyUpdateFreq_Click);
            // 
            // stylingToolStripMenuItem
            // 
            this.stylingToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Colors,
            this.FontItem});
            this.stylingToolStripMenuItem.Name = "stylingToolStripMenuItem";
            this.stylingToolStripMenuItem.Size = new System.Drawing.Size(55, 19);
            this.stylingToolStripMenuItem.Text = "&Styling";
            // 
            // Colors
            // 
            this.Colors.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BG,
            this.FG});
            this.Colors.Name = "Colors";
            this.Colors.Size = new System.Drawing.Size(152, 22);
            this.Colors.Text = "&Colors";
            // 
            // FG
            // 
            this.FG.Name = "FG";
            this.FG.Size = new System.Drawing.Size(152, 22);
            this.FG.Text = "&Foreground";
            this.FG.Click += new System.EventHandler(this.FG_Click);
            // 
            // BG
            // 
            this.BG.Name = "BG";
            this.BG.Size = new System.Drawing.Size(152, 22);
            this.BG.Text = "&Background";
            this.BG.Click += new System.EventHandler(this.BG_Click);
            // 
            // FontItem
            // 
            this.FontItem.Name = "FontItem";
            this.FontItem.Size = new System.Drawing.Size(152, 22);
            this.FontItem.Text = "&Font";
            this.FontItem.Click += new System.EventHandler(this.FontItem_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.LogBox);
            this.Controls.Add(this.StripTop);
            this.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.MainMenuStrip = this.StripTop;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Main";
            this.ShowIcon = false;
            this.Text = "Call of Chernobyl RP Console";
            this.Load += new System.EventHandler(this.Main_Load);
            this.StripTop.ResumeLayout(false);
            this.StripTop.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LogBox;
        private System.Windows.Forms.MenuStrip StripTop;
        private System.Windows.Forms.ToolStripMenuItem Debug;
        private System.Windows.Forms.ToolStripMenuItem ExitRP;
        private System.Windows.Forms.ToolStripMenuItem ExitBoth;
        private System.Windows.Forms.ToolStripMenuItem ClearLog;
        private System.Windows.Forms.ToolStripSeparator DebugSep;
        private System.Windows.Forms.ToolStripMenuItem UpdateFreqHeader;
        private System.Windows.Forms.ToolStripTextBox UpdateFreq;
        private System.Windows.Forms.ToolStripMenuItem ApplyUpdateFreq;
        private System.Windows.Forms.ToolStripMenuItem stylingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Colors;
        private System.Windows.Forms.ToolStripMenuItem BG;
        private System.Windows.Forms.ToolStripMenuItem FG;
        private System.Windows.Forms.ToolStripMenuItem FontItem;
    }
}

