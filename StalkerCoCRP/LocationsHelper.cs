﻿using System.Collections.Generic;
namespace Taconator.CoCRP
{
    /// <summary>
    /// Class for converting the in-game names of levels to their real names
    /// </summary>
    public static class LocationsHelper
    {
        /// <summary>
        /// A dictionary of all locations in S.T.A.L.K.E.R.: Call of Chernobyl indexed by their in-game names
        /// </summary>
        public static readonly Dictionary<string, string> Locations = new Dictionary<string, string> {
            { "k00_marsh", "Great Swamp" },
            { "l01_escape", "Cordon" },
            { "l02_garbage", "Garbage" },
            { "l03_agroprom", "Agroprom" },
            { "k01_darkscape", "Darkscape" },
            { "l04_darkvalley", "Dark Valley" },
            { "l05_bar", "Rostok" },
            { "l06_rostok", "Wild Territory" },
            { "l07_military", "Army Warehouses" },
            { "l08_yantar", "Yantar" },
            { "l09_deadcity", "Dead City" },
            { "l10_limansk", "Limansk" },
            { "l10_radar", "Radar" },
            { "l10_red_forest", "Red Forest" },
            { "l11_hospital", "Deserted Hospital" },
            { "l11_pripyat", "Pripyat" },
            { "l12_stancia", "Chernobyl NPP (South)" },
            { "l12_stancia_2", "Chernobyl NPP (North)" },
            { "l13_generators", "Generators" },
            { "l03u_agr_underground", "Agroprom Underground" },
            { "l04u_labx18", "Lab X-18" },
            { "l08u_brainlab", "Lab X-16" },
            { "l10u_bunker", "Lab X-19" },
            { "l12u_sarcofag", "Sarcophagus" },
            { "l12u_control_monolith", "Monolith Control Center" },
            { "l13u_warlab", "Monolith War Lab" },
            { "zaton", "Zaton" },
            { "jupiter", "Jupiter" },
            { "jupiter_underground", "Jupiter Underground" },
            { "pripyat", "Pripyat Outskirts" },
            { "labx8", "Lab X-8" },
            { "k02_trucks_cemetery", "Truck Cemetery" }
        };

        /// <summary>
        /// Same as accessing a value from the dictionary but with a method call instead
        /// </summary>
        /// <param name="Loc">Location ID</param>
        /// <returns>Location name</returns>
        public static string GetLocation(string Loc) => (Locations.TryGetValue(Loc.ToLower(), out string Location)) ? Location : Loc;
    }
}
