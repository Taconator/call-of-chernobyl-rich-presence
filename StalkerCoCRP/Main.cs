﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace Taconator.CoCRP
{
    public partial class Main : Form
    {
        /// <summary>
        /// The actual CoC process
        /// </summary>
        Process Stalker;
        public Main()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Occurs when CoC is closed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void StalkerClosed(object sender, System.EventArgs e) => Cleanup();

        /// <summary>
        /// Disposes of the Rich Presence Controller and closes this application
        /// </summary>
        public void Cleanup()
        {
            IM.ControllerInstance.Dispose();
            Application.Exit();
        }

        /// <summary>
        /// Logs something to the listbox on this form
        /// </summary>
        /// <param name="Text">Text to log</param>
        public void Log(string Text)
        {
            if (IM.MainWindow.LogBox.InvokeRequired)
            {
                IM.MainWindow.LogBox.Invoke(new MethodInvoker(delegate { IM.MainWindow.LogBox.Items.Add(Text); }));
            }
            else
            {
                IM.MainWindow.LogBox.Items.Add(Text);
            }

        }

        /// <summary>
        /// Load event for this form, adds this to the Instance Manager, creates a new <seealso cref="Controller"/> instance, detects CoC and starts it if necessary, then attaches the Exit event to it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Main_Load(object sender, System.EventArgs e)
        {
            IM.MainWindow = this;
            IM.ControllerInstance = new Controller();
            if (Process.GetProcessesByName("xrEngine").Length == 0)
            {
                Log("CoC not open so opening it now");
                Process.Start(System.IO.Path.Combine(System.Environment.CurrentDirectory, "Stalker-CoC.exe"), "-dbg");
                while (Process.GetProcessesByName("xrEngine").Length < 1)
                {
                    System.Threading.Thread.Sleep(500);
                }
            } else
            {
                Log("CoC already open, not opening again");
            }
            Stalker = Process.GetProcessesByName("xrEngine")[0];
            Stalker.EnableRaisingEvents = true;
            Stalker.Exited += StalkerClosed;
            Stalker.Disposed += StalkerClosed;
            Log("Got X-Ray Engine process and attached exit event");
            UpdateFreq.Text = (IM.UpdateFrequency / 1000).ToString();
            LoadStyling();
        }

        /// <summary>
        /// Defunct
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SavesPath_Click(object sender, System.EventArgs e) => Log(IM.ControllerInstance.RPFileWatcher.Path);

        /// <summary>
        /// Closes just this program, leaving CoC open
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitRP_Click(object sender, System.EventArgs e) => Cleanup();

        /// <summary>
        /// Closes both this program and CoC
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitBoth_Click(object sender, System.EventArgs e)
        {
            Stalker.Kill();
            Cleanup();
        }

        private void ClearLog_Click(object sender, System.EventArgs e)
        {
            LogBox.Items.Clear();
        }

        private void ApplyUpdateFreq_Click(object sender, System.EventArgs e)
        {
            if (float.TryParse(UpdateFreq.Text, out float UpdateFrequency))
            {
                IM.UpdateFrequency = (int)(UpdateFrequency * 1000);
                Log("Update Frequency updated to " + IM.UpdateFrequency.ToString() + " milliseconds");
            }
            else MessageBox.Show("Value entered was not a number", "Failed to Change Update Frequency", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void LoadStyling()
        {
            List<Color> WhiteBlack = new List<Color>() { Color.White, Color.Black };
            Color StripFore = WhiteBlack[ColorHelpers.closestColor2(WhiteBlack, StripTop.BackColor)];
            foreach (Control Ctrl in Controls)
            {
                Ctrl.ForeColor = Properties.Settings.Default.Foreground;
                Ctrl.BackColor = Properties.Settings.Default.Background;
            }

            foreach (ToolStripItem Ctrl in StripTop.Items)
            {
                Ctrl.ForeColor = StripFore;
                Ctrl.BackColor = ControlPaint.Dark(Properties.Settings.Default.Background, .05f);
            }
            FG.BackColor = Properties.Settings.Default.Foreground;
            BG.BackColor = Properties.Settings.Default.Background;
            FG.ForeColor = Color.FromArgb(Properties.Settings.Default.Foreground.ToArgb() ^ 0xffffff);
            BG.ForeColor = Color.FromArgb(Properties.Settings.Default.Background.ToArgb() ^ 0xffffff);
            StripTop.BackColor = ControlPaint.Dark(Properties.Settings.Default.Background, .01f);
            Font = Properties.Settings.Default.Font;
        }

        private void BG_Click(object sender, System.EventArgs e)
        {
            ColorDialog ColorPicker = new ColorDialog() { AllowFullOpen = true, AnyColor = true, FullOpen = true, SolidColorOnly = true, ShowHelp = true };
            if (ColorPicker.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.Background = ColorPicker.Color;
                Properties.Settings.Default.Save();
                LoadStyling();
            }
        }

        private void FG_Click(object sender, System.EventArgs e)
        {
            ColorDialog ColorPicker = new ColorDialog() { AllowFullOpen = true, AnyColor = true, FullOpen = true, SolidColorOnly = true, ShowHelp = false };
            if (ColorPicker.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.Foreground = ColorPicker.Color;
                Properties.Settings.Default.Save();
                LoadStyling();
            }
        }

        private void FontItem_Click(object sender, System.EventArgs e)
        {
            FontDialog FontPicker = new FontDialog() { AllowScriptChange = true, AllowSimulations = true, AllowVectorFonts = true, AllowVerticalFonts = false, Color = ForeColor, Font = Font, FixedPitchOnly = true, FontMustExist = true, ShowApply = true, ShowColor = false, ShowEffects = true, ShowHelp = false };
            if (FontPicker.ShowDialog() == DialogResult.OK)
            {
                Properties.Settings.Default.Font = FontPicker.Font;
                LoadStyling();
            }
        }
    }
}
